import assert from 'assert';
import {
    isPalindrome
} from './index.js';
describe('Строка должна быть палиндромом', () => {
    it('Тест 1', () => {
        const str = '11211';
        assert.ok(isPalindrome(str));
    });
    it('Тест 2', () => {
        const str = 'abcghgcba';
        assert.ok(isPalindrome(str));
    });
    it('Тест 3', () => {
        const str = '';
        assert.ok(isPalindrome(str));
    });
});
describe('Строка не должна быть палиндромом', () => {
    it('Тест 1', () => {
        const str = '123456';
        assert(isPalindrome(str) === false);
    });
    it('Тест 2', () => {
        const str = 'abaaabbb';
        assert(isPalindrome(str) === false);
    });
    it('Тест 3', () => {
        const str = 'adsadadadaaaadaddad';
        assert(isPalindrome(str) === false);
    });
});
