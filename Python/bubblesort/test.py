import unittest

from index import bubble_sort

class Test(unittest.TestCase):
    def test_case_1(self):
        actual = [6, 5, 4, 3, 2, 1]

        expected = [1, 2, 3, 4, 5, 6]
    
        bubble_sort(actual)

        self.assertEqual(actual, expected)
    
    def test_case_2(self):
        actual = [10, 9, 8, 7]

        expected = [7, 8, 9, 10]
    
        bubble_sort(actual)

        self.assertEqual(actual, expected)

    def test_case_3(self):
        actual = [15, 14, 13, 12, 11, 10]

        expected = [10, 11, 12, 13, 14, 15]

        bubble_sort(actual)

        self.assertEqual(actual, expected)

if __name__ == '__main__':
    unittest.main()
